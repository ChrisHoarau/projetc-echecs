#include <iostream>
#include <string>
#include "Joueur.h"

using namespace std;

Joueur::~Joueur()
{
	// string couleur = isWhite() ? "blanc" : "noir";
	// cout << "Destruction de joueur " << couleur << endl;

	vector<Piece*>::iterator p = m_pieces.begin();
	while ( p != m_pieces.end() )
	{
		delete *p;
		p++;
	}
}

Joueur::Joueur()
{
}

Joueur::Joueur(bool white)
{
	init(white);
}

void
Joueur::init(bool white)
{
	Roi *pro = new Roi(white);
	Reine *pre = new Reine(white);
	Cavalier *pcl = new Cavalier(white,true);
	Cavalier *pcr = new Cavalier(white,false);
	Fou *pfl = new Fou(white,true);
	Fou *pfr = new Fou(white,false);
	Tour *ptl = new Tour(white,true);
	Tour *ptr = new Tour(white,false);

	for (int i=1; i<=8; i++)
	{
		Pion *p = new Pion(i,white);
		m_pieces.push_back(p);
	}
	m_pieces.push_back(pro);
	m_pieces.push_back(pre);
	m_pieces.push_back(pcl);
	m_pieces.push_back(pcr);
	m_pieces.push_back(pfl);
	m_pieces.push_back(pfr);
	m_pieces.push_back(ptl);
	m_pieces.push_back(ptr);
}

void
Joueur::affiche()
{
	vector<Piece*>::iterator p = m_pieces.begin();
	while ( p != m_pieces.end() )
	{
		(*p)->affiche();
		p++;
	}
}

void
Joueur::placerPieces(Echiquier & e)
{

	vector<Piece*>::iterator p = m_pieces.begin();
	while ( p != m_pieces.end() )
	{
		e.placer(*p);
		p++;
	}
}

JoueurBlanc::JoueurBlanc() :Joueur(true) { }

bool
JoueurBlanc::isWhite()
{
	return true;
}

JoueurNoir::JoueurNoir() :Joueur(false) { }

bool
JoueurNoir::isWhite()
{
	return false;
}
