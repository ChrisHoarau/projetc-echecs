/** 
 * Header de Piece.cxx
 *
 * @file Piece.h
 */

#if !defined Piece_h
#define Piece_h

//#include "Echiquier.h"
class Echiquier;

/**
 * Declaration d'une classe modélisant une piece de jeu d'echec.
 */
class Piece 
{
protected:
  int m_x;
  int m_y;
  bool m_white;
  
public:
  Piece();
  Piece( int x, int y, bool white );
  virtual ~Piece();
  void init( int x, int y, bool white );
  void move( int x, int y );
  int x();
  int y();
  bool compare(Piece & p);
  bool isWhite();
  bool isBlack();
  void affiche();
  virtual bool mouvementValide(Echiquier &e, int x, int y);
  virtual char type();
};

class Tour : virtual public Piece
{
public:
  Tour();
  Tour(bool white, bool left);
  bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};

class Fou : virtual public Piece
{
public:
  Fou();
  Fou(bool white, bool left);
  virtual bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};

class Cavalier : public Piece
{
public:
  Cavalier(bool white, bool left);
  virtual bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};

class Reine : public Fou, public Tour
{
public:
  Reine(bool white);
  bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};

class Roi : public Piece
{
public:
  Roi(bool white);
  bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};

class Dame : public Piece
{
public:
  Dame(bool white);
  bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};

class Pion : public Piece
{
public:
  Pion(int x, bool white);
  bool mouvementValide(Echiquier &e, int x, int y);
  char type();
};
#include "Echiquier.h"
#endif // !defined Piece_h
