/** 
 * Mise en oeuvre de Piece.h
 *
 * @file Piece.cxx
 */

// A besoin de la declaration de la classe
#include "Piece.h"
//On a déjà inclus Echiquier.h *à la fin* de Piece.h
//#include "Echiquier.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

Piece::Piece()
{
  // cout << "une pièce créée sans coordonnées" << endl;
  // ne fait rien => objet instancie mais non valide.
}

Piece::~Piece()
{
  // cout << "une pièce détruite de coordonnées:" << endl;
  // affiche();
}

Piece::Piece( int x, int y, bool white )
{
  //cout << "une pièce créée avec coordonnées" << endl;
  init(x,y,white);
}

Roi::Roi(bool white) : Piece(5,(white?1:8),white) { }

Reine::Reine(bool white) : Piece(4,(white?1:8),white) { }

Cavalier::Cavalier(bool white, bool left) : Piece((left?2:7),(white?1:8),white) { }

Fou::Fou() { }
Fou::Fou(bool white, bool left) : Piece((left?3:6),(white?1:8),white) { }

Tour::Tour() { }
Tour::Tour(bool white, bool left) : Piece((left?1:8),(white?1:8),white) { }

Pion::Pion(int x, bool white) : Piece(x, (white?2:7), white) { }

void
Piece::init( int x, int y, bool white )
{
  m_x = x;
  m_y = y;
  m_white = white;
}

void
Piece::move( int x, int y )
{
  m_x = x;
  m_y = y;
}

int
Piece::x()
{
  return m_x;
}

int
Piece::y()
{
  return m_y;
}

bool
Piece::compare(Piece & p)
{
  if (p.x() == this->m_x && p.y() == this->m_y && this->isWhite() == p.isWhite())
  {
    cout << "Pièces identiques" << endl;
  } else {
    cout << "Pièces différentes" << endl;
  }
}

bool
Piece::isWhite()
{
  return m_white;
}

bool
Piece::isBlack()
{
  return !m_white;
}

void
Piece::affiche()
{
  cout << "(" << x() << ", " << y() << ") : "
       << ( isWhite() ? "B" : "N" ) << endl;
}

bool
Piece::mouvementValide(Echiquier &e, int x, int y)
{
  cout << "Mouvement Valide Generique" << endl;
  return true;
}

bool 
Roi::mouvementValide(Echiquier &e, int x, int y)
{
  //si la case souhaité fait partie des cases disponible du roi
  //(1 case de lui tous autour)
  if (this->m_x + 1 == x && this->m_y == y ||
      this->m_x + 1 == x && this->m_y + 1 == y ||
      this->m_x + 1 == x && this->m_y - 1 == y ||
      this->m_x - 1 == x && this->m_y == y ||
      this->m_x - 1 == x && this->m_y + 1 == y ||
      this->m_x - 1 == x && this->m_y - 1 == y ||
      this->m_x == x && this->m_y + 1 == y ||
      this->m_x == x && this->m_y - 1 == y) {
    //s'il n'y a aucune piece sur la case
    if (e.getPiece(x, y) == 0) {
      return true;
    }else {
      //s'il y a une pièce sur la case
      //et si la pièce sur cette case est de couleur différente à la pièce qu'on déplace
       if (e.getPiece(x, y)->isWhite() != this->isWhite()) {
        return true;
      }
    }
  }
  return false;
}

bool
Reine::mouvementValide(Echiquier &e, int x, int y)
{
  return Tour::mouvementValide(e, x, y) || Fou::mouvementValide(e, x, y);
}

bool
Cavalier::mouvementValide(Echiquier &e, int x, int y)
{
  //si la case souhaité fait partie des cases disponibles du cavalier 
  //(2 cases et 1 case sur un côté)
  if (this->m_x + 2 == x && this->m_y + 1 == y ||
      this->m_x + 2 == x && this->m_y - 1 == y ||
      this->m_x - 2 == x && this->m_y + 1 == y ||
      this->m_x - 2 == x && this->m_y - 1 == y ||
      this->m_x + 1 == x && this->m_y + 2 == y ||
      this->m_x - 1 == x && this->m_y + 2 == y ||
      this->m_x + 1 == x && this->m_y - 2 == y ||
      this->m_x - 1 == x && this->m_y - 2 == y) {
    //s'il n'y a aucune piece sur la case
    if (e.getPiece(x, y) == 0) {
      return true;
    }else {
      //s'il y a une pièce sur la case
      //et si la piece sur cette case est de couleur différente à la pièce qu'on déplace
      if (e.getPiece(x, y)->isWhite() != this->isWhite()) {
        return true;
      }
    }
  }
  return false;
}

bool
Fou::mouvementValide(Echiquier &e, int x, int y) // A FAIRE !!!
{
  //permet la recherche d'obstacle entre la case actuel et la case ou l'on veut se déplacer
  int rech_x = m_x;
  int rech_y = m_y;

  bool obstacle = false;
  //si la case souhaité fait partie des cases disponibles du fou
  //(en diagonale)
  int dif_x = abs(this->m_x - x);
  int dif_y = abs(this->m_y - y);
  if (dif_x == dif_x) {
    int dir;
    //trouver la direction du déplacement (si x+ et y+ ou x+ et y- ou x- et y+ ou x- et y-)
    if (x > 0 && y > 0) {
      dir = 1;
      rech_x++;
      rech_y++;
    }else {
      if (x > 0 && y < 0) {
        dir = 2;
        rech_x++;
        rech_y--;
      }else {
        if (x < 0 && y > 0) {
          dir = 3;
          rech_x--;
          rech_y++;
        }else {
          dir = 4;
          rech_x--;
          rech_y--;
        }
      }
    }
    //recherche d'obstacle
    while (!obstacle && abs(x - rech_x) > 0 && abs(y - rech_y) > 0) {
      if (e.getPiece(rech_x, rech_y) != 0) {
        obstacle = true;
      }
      switch (dir) {
        case 1 :
          rech_x++;
          rech_y++;
        break;
        case 2 :
          rech_x++;
          rech_y--;
        break;
        case 3 :
          rech_x--;
          rech_y++;
        break;
        case 4 :
          rech_x--;
          rech_y--;
        break;
      }                 
    }

    if (obstacle) {
      return false;
    }else {
      //s'il n'y a aucune piece sur la case
      if (e.getPiece(x, y) == 0) {
        return true;
      }else {
        //s'il y a une piece sur la case
        //et si la piece sur cette case est de couleur différente à la piece qu'on déplace
        if (e.getPiece(x, y)->isWhite() != this->isWhite()) {
          return true;
        }
      }
    }
  }
  return false;
}

bool
Tour::mouvementValide(Echiquier &e, int x, int y)
{
  bool obstacle = false;
  //s'il se déplace sur une ligne (sur les x)
  if (this->m_x == x) {
    for (int i = 1; i <= abs(this->m_y - y); i++) {
      if (e.getPiece(x, i) != 0) {
        obstacle = true;
      }
    }
  }else {
    //s'il se déplace sur une colonne (sur les y)
    if (this->m_y == y) {
      for (int i = 1; i <= abs(this->m_x - x); i++) {
        if (e.getPiece(i, y) != 0) {
          obstacle = true;
        }
      }
    }
  }
  //s'il y a un obstacle sur la trajectoire retourne false sinon retourne true
  return obstacle ? false : true;
}

bool
Pion::mouvementValide(Echiquier &e, int x, int y)
{
  //s'il n'y a aucune pièce sur la case où l'on veut se déplacer
  if (e.getPiece(x, y) != 0) {
    if (this->isWhite() && this->m_y + 1 == y) {
      return true;
    }else {
      if (this->isBlack() && this->m_y - 1 == y) {
        return true;
      }
    }
  //s'il y a une pièce sur la case où l'on veut se déplacer
  }else {
    //si les deux pièce sont de couleur différente (noir et blanc)
    if (e.getPiece(x, y)->isWhite() != this->isWhite()) {
      if (this->isWhite() && this->m_y + 1 == y && (this->m_x + 1 == x || this->m_x - 1 == x)) {
        return true;
      }else {
        if (this->isBlack() && this->m_y - 1 == y && (this->m_x + 1 == x || this->m_x - 1 == x)) {
          return true;
        }
      }
    } 
  } 
  
  return false;
}

char
Piece::type()
{
  return m_white ? 'B' : 'N';
}

char
Roi::type()
{
  return m_white ? 'R' : 'r';
}

char
Reine::type()
{
  return m_white ? 'Q' : 'q';
}

char
Cavalier::type()
{
  return m_white ? 'C' : 'c';
}

char
Fou::type()
{
  return m_white ? 'F' : 'f';
}

char
Tour::type()
{
  return m_white ? 'T' : 't';
}

char
Pion::type()
{
  return m_white ? 'P' : 'p';
}
