#if !defined Joueur_h
#define Joueur_h

#include "Piece.h"
#include "Echiquier.h"
#include <vector>

class Joueur 
{
private:
	// Piece *m_pieces[16];
        std::vector<Piece *> m_pieces;

public:
	~Joueur();
	Joueur( bool white );
	Joueur();

	void init( bool white );
	void affiche();
	void placerPieces(Echiquier & e);
};

class JoueurBlanc : public Joueur
{
public:
        JoueurBlanc();
	bool isWhite();
};

class JoueurNoir : public Joueur
{
public:
        JoueurNoir();
	bool isWhite();
};
#endif // !defined Joueur_h
