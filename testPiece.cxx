/** 
 * Programme test de Piece
 *
 * @file testPiece.cxx
 */

// Utile pour l'affichage
#include <iostream>
#include <assert.h>
#include "Piece.h"
#include "Joueur.h"
#include "Echiquier.h"

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
  // instancie un objet p1 de type Piece
  Piece p1;
  // p1 est une piece blanche de coordonnees (3,3)
  p1.init( 3, 3, true );

  //On l'affiche
  // p1.affiche();

  Piece p2(5, 5, false);
  // p2.affiche();

  // p1.compare(p2);

  Piece tab[4];

  // les objets definis dans cette fonction sont automatiquement d�truits.
  // Ex : p1

  // Joueur jblanc(true);
  // Joueur jnoir(false);

  JoueurBlanc jblanc;
  JoueurNoir jnoir;

  cout << boolalpha << "Le joueur blanc est bien blanc ? " << jblanc.isWhite() << endl;
  cout << boolalpha << "Et le joueur noir, est-il blanc ? " << jnoir.isWhite() << endl;

  jblanc.affiche();
  jnoir.affiche();

  Echiquier e;
  e.affiche();

  //place les pieces des joueurs sur l'echiquier
  jblanc.placerPieces(e);
  jnoir.placerPieces(e);

  //affiche l'echiquier � l'initialisation de la partie
  e.affiche();

  bool findugame = false;
  int x, y;
  while (!findugame) {
    //indique la pi�ce � d�placer
    cout << "Joueur blanc\nPi�ce � d�placer, x = ";
    cin >> x;
    cout << "y = ";
    cin >> y;
    
  }
}
